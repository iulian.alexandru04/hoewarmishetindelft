The python script might contain more code than necessary, but I wanted to showcase a basic example of polymorphism (by implementing 3 custom exceptions)

I have validated the response according to a comment from 'ajaxLoader' function, responsible for updating the temperature
which specifies that a "valid clientraw.txt has '12345' at start and '!!' at end of record"

Since temperature might change position at some point, I checked the API version as well
(other comment mentions that the file contains a version with the form !!nn.nn!!)


I chose to use kaniko for building the docker image because although possible, building it inside a docker container requires enabling '--docker-priviledge'
which, according to the gitlab docs, can lead to container breakout.

Docker images will automatically be pushed to gitlab registries when tags are pushed to repository.

