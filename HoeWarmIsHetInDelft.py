import requests
import sys
import time


class BadHttpResponse(Exception):
    def __init__(self, response):
        super().__init__(f'Bad HTTP response: {response}')


class InvalidRawData(Exception):
    def __init__(self):
        super().__init__('Invalid raw data')


class InvalidApiVersion(Exception):
    def __init__(self, api_version):
        super().__init__(f'Invalid API version: {api_version}')


def get_temperature():
    version_string = '!!C10.37S103!! '
    crt_time = int(time.time())
    query = '?' + str(crt_time)
    url = 'https://www.weerindelft.nl/clientraw.txt' + query
    response = requests.get(url)
    if not response:
        raise BadHttpResponse()

    if not (response.content[:5] == b'12345' and response.content[-3:] == b'!! '):
        raise InvalidRawData()

    content = str(response.content, 'utf-8')
    if not content.endswith(version_string):
        raise InvalidApiVersion()

    data = content.split(' ')
    temperature = float(data[4])
    return round(temperature)


if __name__ == '__main__':
    try:
        temperature = get_temperature()
        print(f'{temperature} degrees Celsius')
    except (BadHttpResponse, InvalidRawData, InvalidApiVersion) as err:
        sys.exit(err)

